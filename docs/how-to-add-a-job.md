---
title: How to add a job
description: You want to add you job in R2Devops? You are in the right place! Find out easily how to add your job and help to develop this platform
---

# How to

Glad to see you here 🥳 

They are 2 ways to add a job into R2Devops. This page will guide you to the right place depending on the way you want to add a job. 

!!! important "For both you need to understand the [structure of a job](./job-structure.md)"

Then, you can:

* **Link your job directly from your GitLab repository** 👉 Go to [link a job](./link-a-job.md).
* **Contribute on the official repository** 👉 Go to [contribute](./contribute.md).

!!! heart "Community"
    We love talking with our contributors and users! Join our
    [Discord community :fontawesome-brands-discord:](https://discord.r2devops.io/?utm_medium=website&utm_source=r2devopsdocumentation?utm_campaign=addajob)
