---
title: Concept of R2Devops, collaborative hub of CI/CD jobs
description: Discover R2Devops and learn about the goal of the hub. See how CI/CD implementation can be simplified using this platform!
---


# Concept

The **R2Devops hub** is a collaborative [Hub](/r2bulary/#hub) of CI & CD
**ready to use** jobs which helps you to easily build powerful Pipelines for your projects.

!!! info
    Currently, the hub is focused to provide only **Gitlab 🦊** jobs. We plan
    to support more CI/CD platforms in the future.

Each Job of the hub can be used independently to create fully **customized pipelines.**
You can use them for any kind of software and deployment type. Each job can be
customized through configuration.

!!! info "Some jobs are plug and play"

     It means that you can used them without configuration: you juste have to include them and they'll work!

<a alt="Use the hub" href="/use-the-hub">
    <button class="md-button border-radius-10 md-button-center" >
        I want to use the hub <img alt="" class="heart" src="../images/rocket.png">
    </button>
</a>


## Overview

![hub overview](images/hub.png)

--8<-- "includes/abbreviations.md"
