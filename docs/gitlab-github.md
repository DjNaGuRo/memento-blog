# GitLab CI/CD to GitHub Actions jobs' conversion

GitLab CI/CD and Github Actions both allow us to create pipelines and workflows respectively, to build, test, publish, release and deploy projects. They are write in `.yml` files.
## CI/CD files location
| GitLab CI/CD | GitHub Actions |
| -------------| ---------------|
| Unique entrypoint : `.gitlab-ci.yml` file in the root of the repository. Others CI/CD files can be located anywhere in the repository | No entrypoint. CI/CD files must be placed in `.github/workflows/` folder. Any workflow in that folder will be execute when the trigger condition is satisfied |

## Workflow and pipeline structure
### GitHub Actions
```yml
name: <workflow name> # Optional
on: <one or more event that trigger the workflow> 
jobs: # section where to define all jobs of the workflow
    first-job: # job ID / name to identify it
        name: <text to shortly describe the job purpose> # Optional
        runs-on: <[ubuntu|windows|macos]-version> # Type of machine to run the job on
        steps: # Instructions and Actions to run in the job
            - name: Checkout 
              uses: actions/checkout@v2 # Alwalys used to import repository content inside the runner environment
            - uses: <{action name}@{ref}> # acction to run
            - run: <shell cmd> # Command to execute
    second-job:
        ...
```
### GitLab CI/CD
```yml
stages: # Optional
    - stage-1
    - stage-n
first-job: # Job ID / name to identify it
    stage: <stage-name> # Stage to which the job is associated
    scripts: # Set of commands to execute when the the job runs
     - an instruction 
     - another instruction
```



## GitLab CI/CD - GitHub Actions keywords correspondances
| GitLab CI/CD | GitHub Actions |
| ---          | ---            |
| `variables:` to define pipeline global variables and job's variables| `env:` # to define workflow (top-level) and job's variables (job-level and step-level)|
`tags:` to define the OS of the runner | `runs-on:` to specify runner OS
`image:` for using docker image | `container:` for docker image usage
`rules:` use for conditional execution | `if:` for conditional execution
`stages:` create precedence between sets of jobs | No equivalence. Use `needs:` to perform dependance between jobs
`stage:` | No equivalent keyword nor concept
`cache:` for caching pipeline files | use action `actions/cache` to create cache
`artifacts:` to persist data as artifacts | use action `actions/upload-artifact` 

!!! caution "Variables manipulation and exposure"

    In **GitLab CI/CD**, variables are updated in `script:` section just by assigning them new values (**Syntax:** `<variable_name>=<new_value>`) and the updated values are immediately available. 
    But in **GitHub Actions**, to create or update a variable in `steps:` section we have to store the result of the assigment statement in the global environment space `GITHUB_ENV` by using the following syntax  `echo "<variable_name>=<value>" >> $GITHUB_ENV`. And the created variables or updated values aren't available within the current step but only for the subsequent ones.

!!! warning "Script vs Steps behaviors"

    In **GitLab CI/CD**, all steps of a job - items of the `script:` section, are run on the same execution environment so that all modifications made by a step are visible by default and affect the others.
    But in **GitHub Actions**, each step of a job - items of `steps:` section, is run in a fresh environment so the modifications done in a step don't affect the context of the other steps. [See more in GitHub doc]()

## Exemple : GitLab CI/CD conf to GitHub Action conf
**GitLab CI/CD conf to convert**
```yml 
cargo_doc:
    image: rust:1.57-buster
    stage: build
    variables:
      ONLY_LIB: 'false'
      PROJECT_ROOT: '.'
      RELEASE_MODE: 'true'
      OUTPUT_FOLDER: "website_build"
      ADDITIONAL_OPTIONS: ''
    script:
      - cd $PROJECT_ROOT
      - if [ ! -f "Cargo.toml" ]; then
      -   echo "ERROR --> Any Cargo.toml file isn't present in the given folder $PROJECT_ROOT"
      -   exit 1
      - fi
      - if [ ${ONLY_LIB} == "true" ]; then
      -   ADDITIONAL_OPTIONS+=" --lib"
      - fi
      - if [ ${RELEASE_MODE} == "true" ]; then
      -   ADDITIONAL_OPTIONS+=" --release"
      - fi
      - if [ ! ${CI_PROJECT_DIR} -ef ${PROJECT_ROOT} ]; then
      -   OUTPUT_FOLDER="${CI_PROJECT_DIR}/${OUTPUT_FOLDER}"
      - fi
      - cargo doc --target-dir ${OUTPUT_FOLDER} $ADDITIONAL_OPTIONS
    artifacts:
      when: always
      expose_as: "Cargo_doc build"
      paths:
        - $OUTPUT_FOLDER

```

### Variable section
GitLab CI/CD 
```yml
cargo_doc:
    ...
    variables:
        ONLY_LIB: 'false'
        PROJECT_ROOT: '.'
        RELEASE_MODE: 'true'
        OUTPUT_FOLDER: "website_build"
        ADDITIONAL_OPTIONS: ''
```

GitHub Actions : *Variables are moved up from job-level (in GitLab CI/CD conf) to workflow-level as inputs (in GitHub Actions conf)*
```yml
name: Cargo doc workflow

on:
  workflow_call: # REQUIRED to make the workflow callable from within another
    inputs:
      PROJECT_ROOT:
        description: Path to th project to document # description is mandatory
        type: string # type is mandatory
        default: '.' # default is optional
      ONLY_LIB:
        description: If document package's library
        type: boolean
        default: false
      RELEASE_MODE:
        description: If document optimized artifacts with the release profile
        type: boolean
        default: true
      OUTPUT_FOLDER:
        description: Path where to save output documentation
        type: string
        default: 'website_build'
      ADDITIONAL_OPTIONS:
        description: Additional options space-seperated list to pass to cargo doc command
        type: string
        default: ''
```

### Docker image section
GitLab CI/CD
```yml
cargo_doc:
    image: rust:1.57-buster
    ...
```

GitHub Actions
```yml
jobs:
  cargo_doc:
    ...
    container:
      image: rust:1.57-buster
```

### Script section
GitLab CI/CD
```yml
script:
      - cd $PROJECT_ROOT
      - if [ ! -f "Cargo.toml" ]; then
      -   echo "ERROR --> Any Cargo.toml file isn't present in the given folder $PROJECT_ROOT"
      -   exit 1
      - fi
      - if [ ${ONLY_LIB} == "true" ]; then
      -   ADDITIONAL_OPTIONS+=" --lib" # variable update
      - fi
      - if [ ${RELEASE_MODE} == "true" ]; then
      -   ADDITIONAL_OPTIONS+=" --release"
      - fi
      - if [ ! ${CI_PROJECT_DIR} -ef ${PROJECT_ROOT} ]; then
      -   OUTPUT_FOLDER="${CI_PROJECT_DIR}/${OUTPUT_FOLDER}"
      - fi
      - cargo doc --target-dir ${OUTPUT_FOLDER} $ADDITIONAL_OPTIONS

```

GitHub Actions:
```yml
steps:
      ...
      - run: |
          cd ${{ inputs.PROJECT_ROOT }}
          echo "CIWORKSPACE_PATH=$GITHUB_WORKSPACE" >> $GITHUB_ENV
          echo "PROJECT_ROOT_RELATIVE_PATH=$(pwd)" >> $GITHUB_ENV # Create a new environment variable PROJECT_ROOT_RELATIVE_PATH
      - run: |
          cd ${{ inputs.PROJECT_ROOT }}
          if [ ! -f "Cargo.toml" ]; then
            echo "ERROR --> Any Cargo.toml file isn't present in the given root project folder ${{ inputs.PROJECT_ROOT }}"
          fi
      - run: | # Create copies of inputs variables that need to be updated
          echo "ADDITIONAL_OPTIONS=${{inputs.ADDITIONAL_OPTIONS}}" >> $GITHUB_ENV
          echo "OUTPUT_FOLDER=${{inputs.OUTPUT_FOLDER}}" >> $GITHUB_ENV
      - run: |
          if [ ${{ inputs.ONLY_LIB }} ]; then
            echo "ADDITIONAL_OPTIONS=${{env.ADDITIONAL_OPTIONS}} --lib" >> $GITHUB_ENV # variable update statement
          fi
      - run: |
          if [ ${{ inputs.RELEASE_MODE }} ]; then
            echo "ADDITIONAL_OPTIONS=${{env.ADDITIONAL_OPTIONS}} --release" >> $GITHUB_ENV
          fi
      - run: |
          if [ ! ${{env.PROJECT_ROOT_RELATIVE_PATH}} -ef ${{env.CIWORKSPACE_PATH}} ]; then
            echo "OUTPUT_FOLDER=$GITHUB_WORKSPACE/${{ inputs.OUTPUT_FOLDER }}" >> $GITHUB_ENV
          fi   
      - run: |
          cd ${{ inputs.PROJECT_ROOT }} 
          cargo doc --target-dir ${{env.OUTPUT_FOLDER}} ${{ env.ADDITIONAL_OPTIONS }}
```

### Artifact section
GitLab CI/CD
```yml
artifacts:
      when: always
      expose_as: "Cargo_doc build"
      paths:
        - $OUTPUT_FOLDER
```

GitHub Actions
```yml
- name: Upload artifacts
  uses: actions/upload-artifact@v2
  if: always()
  with:
      name: Cargo_doc build
      path: ${{ env.OUTPUT_FOLDER }}
```

**GitHub Actions conf output**
```yml
name: Cargo doc workflow

on:
  workflow_call:
    inputs:
      PROJECT_ROOT:
        description: Path to th project to document
        type: string
        default: '.'
      ONLY_LIB:
        description: If document package's library
        type: boolean
        default: false
      RELEASE_MODE:
        description: If document optimized artifacts with the release profile
        type: boolean
        default: true
      OUTPUT_FOLDER:
        description: Path where to save output documentation
        type: string
        default: 'website_build'
      ADDITIONAL_OPTIONS:
        description: Additional options space-seperated list to pass to cargo doc command
        type: string
        default: ''
jobs:
  cargo_doc:
    runs-on: ubuntu-latest
    container:
      image: rust:1.57-buster
    steps:
      - name: Checkout 
        uses: actions/checkout@v2 # use to upload the repository content into the runner workspace: GITHUB_WORKSPACE
      - run: |
          cd ${{ inputs.PROJECT_ROOT }}
          echo "CIWORKSPACE_PATH=$GITHUB_WORKSPACE" >> $GITHUB_ENV
          echo "PROJECT_ROOT_RELATIVE_PATH=$(pwd)" >> $GITHUB_ENV # Create a new  environment variable PROJECT_ROOT_RELATIVE_PATH
      - run: |
          cd ${{ inputs.PROJECT_ROOT }} # IMPORTANT to move in the PROJECT_ROOT directory before execution any command
          if [ ! -f "Cargo.toml" ]; then
            echo "ERROR --> Any Cargo.toml file isn't present in the given root project  folder ${{ inputs.PROJECT_ROOT }}"
          fi
      - run: | # Create copies of inputs variables that need to be updated
          echo "ADDITIONAL_OPTIONS=${{inputs.ADDITIONAL_OPTIONS}}" >> $GITHUB_ENV
          echo "OUTPUT_FOLDER=${{inputs.OUTPUT_FOLDER}}" >> $GITHUB_ENV
      - run: |
          if [ ${{ inputs.ONLY_LIB }} ]; then
            echo "ADDITIONAL_OPTIONS=${{env.ADDITIONAL_OPTIONS}} --lib" >> $GITHUB_ENV  # variable update statement
          fi
      - run: |
          if [ ${{ inputs.RELEASE_MODE }} ]; then
            echo "ADDITIONAL_OPTIONS=${{env.ADDITIONAL_OPTIONS}} --release" >>  $GITHUB_ENV
          fi
      - run: |
          if [ ! ${{env.PROJECT_ROOT_RELATIVE_PATH}} -ef ${{env.CIWORKSPACE_PATH}} ];  then
            echo "OUTPUT_FOLDER=$GITHUB_WORKSPACE/${{ inputs.OUTPUT_FOLDER }}" >>  $GITHUB_ENV
          fi   
      - run: |
          cd ${{ inputs.PROJECT_ROOT }} # IMPORTANT to move in the PROJECT_ROOT directory before execution any command
          cargo doc --target-dir ${{env.OUTPUT_FOLDER}} ${{ env.ADDITIONAL_OPTIONS }}
      - name: Upload artifacts
        uses: actions/upload-artifact@v2
        if: always()
        with:
          name: Cargo_doc build
          path: ${{ env.OUTPUT_FOLDER }}
```