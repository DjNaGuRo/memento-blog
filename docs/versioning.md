---
title: Jobs versioning
description: Have you ever wonder how versioning works? We tell you all! You will understand what latest means and how to include properly a job in your CI/CD pipeline.
---

# Versioning

Each Job of the Hub are versioned using a git tag.

!!! note
    Version follows the [Semantic Versioning](https://semver.org/){:target="_blank"}.

You can also use the latest version, using `latest` instead of a tag. Applying
this, you will retrieve the latest version of jobs at each run. Note that if
you don't set any tag, `latest` is used by default.

Each jobs can be used independently with a different version.

Example in `.gitlab-ci.yml`:

```yaml
include:
  - remote: 'https://jobs.r2devops.io/latest/docker.yml'
  - remote: 'https://jobs.r2devops.io/0.1.0/mkdocs.yml'
  - remote: 'https://jobs.r2devops.io/0.1.0/apidoc.yml'
```

Available tags and release note for each job are available in the [jobs section](https://r2devops.io/jobs).

--8<-- "includes/abbreviations.md"
