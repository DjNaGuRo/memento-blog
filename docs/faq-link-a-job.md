---
title: FAQ Link a job
description: Find here all the questions and their answer regarding how to link a job in r2devops.io
---

# Link a job

??? warning "Why do I have an error on my repository URL?"

    They might be several reasons why you have an error message: 
    
    - For now, you can only add a job stored in a Gitlab.com repository. It means the URL of your repository MUST begin with https://gitlab.com/. It’s why we actually fixed it in the first field so you don't have to write it. 
     
    - Your URL can’t contain any special characters, except a dash (-), an underscore(_), a slash (/) or a dot (.).

??? warning "Why do I have an error on the path of my job?"

    If an error message appears once you filled the field for the job path, it can be because you put special characters. In this field, none are allowed, except a dash (-), an underscore (_) and a slash (/).
     
    And be careful: we also fixed the first two characters of the field! It means your input can’t begin with a slash.

??? warning "Why do I have an error on my job name?"

     When giving a name to your job, you can only use lowercase letters, a dash (-), or an underscore (_).

     In addition, the job name must correspond to the job name contained in the yaml file

??? info "Why my job wasn't added?"

    ![jobname error](./images/jobname_error.png)

    Once you passed the firsts steps and filled your path to your job and its name, our crawler is going to analyze the files stored in your repository.
    
    If there is no [jobname.yml]() file in your folder, we won't be able to add your job!

[See **Link a job** documentation](./link-a-job.md)