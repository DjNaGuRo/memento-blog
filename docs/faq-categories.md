---
title: Presentation of the FAQ
description: Find here all the the categories of r2devops.io FAQ
---

# Welcome to the FAQ!

Here you'll find the answers to your question. We try to organize those questions according to main topics:

!!! tip ""

    - [The labels](./faq-labels.md)
    - [Add a job](./faq-add-a-job.md)
    - [Link a job](./faq-link-a-job.md)
    - [Contribute](./faq-contribute.md)
    - [Use a job](./faq-use-a-job.md)

If you can't find the answer to your question, you can contact the support:

<a alt="Use the hub" href="https://discord.r2devops.io?utm_medium=website&utm_source=r2devopsdocumentation&utm_campaign=faq" target="_blank">
    <button class="md-button border-radius-10 md-button-center" >
        On Discord 💬
    </button>
</a>

<a alt="Use the hub" href="https://tally.so/r/w5Edvw" target="_blank">
    <button class="md-button border-radius-10 md-button-center" >
        Open a ticket 🎟
    </button>
</a>
