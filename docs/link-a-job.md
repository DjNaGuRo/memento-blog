---
title: Link a job
description: Here are gathered all the information on how to link a job in R2Devops. Once your job will be added, you'll benefit from R2Devops features!
---

# 🔥 Link a job in R2Devops

Linking a job in R2Devops is one of the two ways to add your job.

This page will explain to you why you should use R2Devops to link a job, and how to do it. You’ll find in the dedicated section all the solutions to error messages that might be displayed during the process of adding a job.

---
## Why link your job?

If you link your job into R2Devops, you will access all the benefits of the hub with no constraint regarding the security and the quality of your code. You can add your job the way you built it and use it!

Using R2Devops to store your job, you will also benefit from the centralization of your jobs. Our platform will retrieve data directly from your files, and automatically file the documentation. 

Your job will be accessible to anyone. It means that any person can use your jobs in his/ her pipeline, and offer improvements or feedbacks on your work!

In order to benefit from R2Devops advantages and link your job, you only have to follow the 4 steps described below.

!!! info "In order to Link a job into R2Devops, your GitLab.com account must be linked to your R2Devops account."

---
## How to link your job?

### Step 1: copy/ paste the URL of your repository 

The first step is to add the GitLab URL of your repository. This URL is the path to the project where your job data are stored, containing the group, the namespace of your project. 

They are 2 ways to find this information:

??? tip "In your terminal"

    You can find it on your Git terminal using the command:

    ```git config --get remote.origin.url```

    Add screen capture for the project hub in R2Devops

??? tip "In GitLab"     

    You can also find this information in GitLab, and copy/ paste the group or namespace and the project name displayed in your browser.

    ![GitLab URL in browser](./images/gitlab-url.png)

!!! failure "I have an error message 🤯"
    There might be several reasons why you have an error message: 

    For now, you can only add a job store in a GitLab repository. It means the URL of your repository MUST begin with ```https://gitlab.com/```. It’s why we actually fixed it in the first field. 

    Your URL can’t contain any special characters, except a dash (-), an underscore(_), a slash (/) or a dot (.).

### Step 2: give us the path to find your job 

Once you gave us the first part of your path, you need to indicate in which folder your job is store.

Same as the first step, they are 2 ways to retrieve this data:

??? tip "In your terminal"

    * Go to your directory containing your job and type in a terminal: ```pwd```
    * Copy/ paste the path after the project name

??? tip "In GitLab"
    
    Just copy/ paste the end of the URL displayed in your browser.

    ![job path in browser](./images/job-path.png)

!!! failure "I have an error message 🤯"

    If an error message appears once you filled the field for the job path, it can be because you put special characters. In
    this field, none are allowed, except a dash (-), an underscore (_) and a slash (/).

    And be careful: we also fixed the first two characters of the field! It means your input can’t begin with a slash.

### Step 3: name the job you want to link 

The final step: give your job a name!

You’ll see, this field completes itself once you put the path to your job will the name of the last folder. Of course, you can rename it the way you want!

!!! failure "I have an error message 🤯"

    When giving a name to your job, you can only use lowercase letters, a dash (-), an underscore (_) or a slash (/).
   
    In addition, the job name must correspond to the job name contained in the yaml file

### Step 4: click on **Import my job** button

Let’s say it, all the previous steps are useless if you don’t click on the button to import your job! Once you did it, a crawler we created will analyze the files stored in your folder.

---
## The files analyzed

In order to add your job in the hub and fill the documentation, our crawler analyzes all the files located in the folder you pointed.
It will retrieve data from 3 of them:

!!! info "the jobname.yml file "

    Here are gathered your job metadata:

    * Job name
    * Job default stage
    * Job docker image

!!! info "the readme.md file"

    The crawler will use the data in this file to display the documentation of your job in our platform.

!!! info "the changelog.md file"

    This file will indicate to the crawler all the versions of your jobs you want to import.

!!! failure "If there is no jobname.yml file in your folder, we won’t be able to import your job and its versions."
    
    We'll be able to import your job if there are no changelog.md or readme.md.

    
