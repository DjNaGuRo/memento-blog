---
title: FAQ Use a job
description: Find here all the questions and their answer regarding how to use a job in r2devops.io
---

# Use a job

??? info "What is a plug and play job?"

    We described as Plug and Play the jobs that don't need configuration in order to work in your pipeline.
    
    It means you can add the include link of the job in your pipeline and directly run it. And it will work ✨


??? info "How do I include a job in my pipeline?"

    In order to include a job in your pipeline, you need to add the include link in your .gitlab-ci.yml file.
    
    But first, you need to precise the stage in which your job is suppose to work!

??? info "How do I delete my job?"
    
    In order to delete a job, you need to access the job's modification page from your contributing dashboard.
    In the bottom of the page, there is a red button to delete your job! You just have to click on it, and follow the steps.

    **Be careful: other people than you might be using your job. If you delete it, their pipelines will broke**