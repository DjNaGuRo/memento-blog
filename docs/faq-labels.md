---
title: FAQ Labels
description: Find here all the questions and their answer regarding the labels of r2devops.io
---

# Labels

??? info "What are the blue labels?"

    Blue labels give information about the origin of the jobs. 

    In R2Devops, blue labels look like this:

    ![Blue label example](./images/label-R2Devops.png){width=20%}
   
    A job can be added in the hub by someone working for **R2Devops** or by someone from **the community**. If you [link a job from your repository](./link-a-job.md), your job will automatically get the label *community*. If you [contribute following R2Devops guidelines](./contribute.md), your job will get the label *R2Devops*!

??? info "What means the label *R2Devops*?"

    ![Blue label R2Devops](./images/label-R2Devops.png){width=20%}
    
    The label R2Devops means that the job respect [certains quality and security standard](https://r2devops.io/create-update-job/#best-practices-optional). 

??? info "What means the label *Community*?"

    ![Blue label community](./images/label-community.png){width=20%}
    
    The label Community means that the job was added by someone from the community. We don't know which quality standards this jobs follow, nore can ensure it's safety.

??? info "What are the green labels?"

    Green labels give technical information about a job. 

    In R2Devops, green labels look like this:

    ![Green label example](./images/label-green.png){width=20%}

??? info "How to add or remove a label?"

    You need to be the owner of a job to add a label. Once you are connected to your account, you can go on your *contributing dashboard*, click on the 3 dots on the right side of a job line and select *Modify my job*

    Once you are on this page, you can add or remove a label from the label input.

??? info "I can't find the label I want to use"
   
    If you can't find the label you want to use, you can [open a ticket](https://tally.so/r/w5Edvw) and ask our team to create a new label!