---
title: FAQ Add a job
description: Find here all the questions and their answer regarding how to add a job in r2devops.io
---

# Add a job

??? info "How can I add a job?"

    They are 2 ways to add a job into R2Devops. You can:

    🔥 [Link your job](/link-a-job)

    ❤️ [Contribute on the official repository](/contribute)
