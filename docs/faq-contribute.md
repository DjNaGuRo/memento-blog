---
title: FAQ Contribute
description: Find here all the questions and their answer regarding how to contribute in r2devops.io
---

# Contribute

??? info "How can I contribute into R2Devops?"

    In order to contribute in R2Devops, you can add a job in the official repository.
    
    To add your job, you need to follow R2Devops guidelines and ensure some quality and security criterias.
    
    All the steps to be listed into R2Devops official repositories are [described in the documentation](/contribute).


??? info "Why my contribution wasn't validated yet?"

      Adding your job into the official R2Devops repository will require more time than linking your job. 
      
      Why?
      
      Because our team personally review your job and ensure it fits our safety and quality requirements! 
      Once the first review is done, they'll let you know your work was perfect, and your job is added or if some adjustments are required.


[See **How to contribute** documentation](/contribute)
