---
title: The crawler
description: Discover which data R2Devops' retrieves in order to import your job into the platform, and how it works.
---

# The crawler

## Definition

A crawler is a bot looking for specific files in a given location on the web.

R2Devops' crawler analyzes the files stored in your GitLab repository in a given folder (`folder_path`). It will look for 3 specific files:

* ```<job_name>.yml``` or ```<job_name>.yaml```
* README.md
* CHANGELOG.md

!!! info "Those 3 files are used to import your job in R2Devops. Only the ```<job_name>.yml``` (or ```<job_name>.yaml```) is mandatory to complete the process."

Once the files are analyzed, the crawler returns:

* A list of versions models for each version found.
* An analysis report with:
     * global warning and errors encountered during analysis.
     * warning and error for each version analyzed.

## The rules

* The user must add a file named ```<job_name>.yml``` OR ```<job_name>.yaml``` at the root of the folder_path.

* If the user wants to use versioning, he has to put a CHANGELOG.md at the root of folder_path, following keepachangelog v1.0.0 format, and use git tags corresponding to version specified in CHANGELOG.md.

* If the repo contains only one job, the git tag can be named following the job version(s) specified in the CHANGELOG.md file.

* If the repo contains several jobs, the git tags have to be named using the job name as prefix like: ```<job_name>-<version>```, with version corresponding to the job version(s) specified in the CHANGELOG.md file.

* If the user wants to document its job, he must add a README.md file at the root of folder_path.

## The workflow

The crawler's analysis happen in two times 👇🏻

### Step 1: retrieve the version list

In this step, the crawler analysis your file and looks for the different versions of your job. No matter if there is a CHANGELOG.md file or not, the crawler will automatically create a version called latest. It corresponds to the actual version of your job, located on the default branch of your repository, independently of any tags. 

![crawler step 1](./images/crawler-step1.png){width=70%}

### Step 2: retrieve data for each version

Once the crawler has detected the versions of your job, we will check if the 3 files needed in order to create your job are available for each of your version. 

![crawler step 2](./images/crawler-step2.png){width=70%}

Once all the versions are analyzed, the crawler gives feedback to R2Devop, and imports the versions of your job available in the platform!

Once your job is imported, you can add additional data, such as labels, the licence and a description. You'll be also able to modify those data from your contributing dashboard.